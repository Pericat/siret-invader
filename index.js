const fs = require('fs');
const csv = require('csv-parser');

async function splitCSV(inputFilePath, outputFolder, batchSize) {
    let currentBatch = 1;
    let currentBatchRecords = [];
    const start = Date.now();


    if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder);
    }

    fs.createReadStream(inputFilePath)
        .pipe(csv())
        .on('data', (row) => {
            currentBatchRecords.push(row);

            if (currentBatchRecords.length >= batchSize) {
                const outputFile = `${outputFolder}/batch_${currentBatch}.csv`;
                const startInsert = Date.now()
                writeCSV(outputFile, currentBatchRecords, startInsert);
                currentBatchRecords = [];
                currentBatch++;
            }
        })
        .on('end', () => {
            if (currentBatchRecords.length > 0) {
                const outputFile = `${outputFolder}/batch_${currentBatch}.csv`;
                writeCSV(outputFile, currentBatchRecords);
            }
            const end = Date.now();
            console.log(`CSV file split into smaller files successfully in ${(end - start)/1000} seconds`);
        })
        .on('error', (error) => {
            console.error('Error parsing CSV:', error);
        });
}

async function writeCSV(outputFile, data, time) {
    const csvData = data.map(row => Object.values(row).join(',')).join('\n');
    fs.writeFileSync(outputFile, csvData);
    const endInsert = Date.now()
    console.log(`insert finished in ${(endInsert - time)/1000} seconds`);
}

const inputFilePath = 'StockEtablissement_utf8.csv'; 
const outputFolder = 'output'; 
const batchSize = 25000; 

splitCSV(inputFilePath, outputFolder, batchSize);
