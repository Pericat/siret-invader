const fs = require('fs');
const readline = require('readline');
const { Worker, isMainThread, workerData } = require('worker_threads');

// Function to parse CSV file and split it into smaller files
async function splitCSV(inputFilePath, outputFolder, batchSize, numWorkers) {
    let currentBatch = 1;
    let currentBatchRecords = [];
    let activeWorkers = 0;

    // Create output folder if it doesn't exist
    if (!fs.existsSync(outputFolder)) {
        fs.mkdirSync(outputFolder);
    }

    // Function to handle processing of CSV records by worker thread
    function handleWorkerTask(records, outputFolder, batchNumber) {
        const outputFile = `${outputFolder}/batch_${batchNumber}.csv`;
        const csvData = records.map(row => Object.values(row).join(',')).join('\n');
        fs.writeFileSync(outputFile, csvData);
    }

    // Function to start a worker thread
    function startWorker(workerData) {
        activeWorkers++;
        const worker = new Worker(__filename, { workerData });
        worker.on('error', (error) => {
            console.error('Worker error:', error);
        });
        worker.on('exit', () => {
            activeWorkers--;
            worker.terminate()
            if (activeWorkers === 0 && currentBatchRecords.length === 0) {
                console.log('All workers completed processing.');
            }
        });
    }

    const rl = readline.createInterface({
        input: fs.createReadStream(inputFilePath),
        crlfDelay: Infinity
    });

    for await (const line of rl) {
        const row = line.split(','); // Assuming CSV is comma-separated, adjust delimiter as needed
        currentBatchRecords.push(row);

        // If the batch size is reached, send records to a worker thread
        if (currentBatchRecords.length >= batchSize) {
            const workerData = {
                batchNumber: currentBatch,
                records: currentBatchRecords,
                outputFolder
            };
            startWorker(workerData);
            currentBatchRecords = [];
            currentBatch++;
        }
    }

    // Process remaining records
    if (currentBatchRecords.length > 0) {
        const workerData = {
            batchNumber: currentBatch,
            records: currentBatchRecords,
            outputFolder
        };
        startWorker(workerData);
    }
    if (!isMainThread) {
        const { records, outputFolder, batchNumber } = workerData;
        handleWorkerTask(records, outputFolder, batchNumber);
    }
}

// Main code for worker thread


// Example usage
const inputFilePath = 'StockEtablissement_utf8.csv'; // Provide your CSV file path
const outputFolder = 'output'; // Provide the folder where you want to save the smaller CSV files
const batchSize = 1000000; // Specify the number of records per smaller file
const numWorkers = 4; // Specify the number of worker threads to use

splitCSV(inputFilePath, outputFolder, batchSize, numWorkers);
